# Given the administrative data, return the model matrix for XGBoost
model_features <- function(data.population, n_ogc=24) {
  # Add oblique coordinates to data.population
  theta <- seq(from=0, to=pi*(n_ogc-1)/n_ogc, by=pi/n_ogc)
  x <- data.population[["x_coord"]]
  y <- data.population[["y_coord"]]
  for (i in 1:n_ogc) {
    xn <- paste("x", i, sep="")
    data.population[[xn]] <- sqrt(x^2 + y^2)*cos(theta[i]-atan(y/x))
  }
  # Transform to a sparse model matrix
  options(na.action='na.pass')
  formula <- as.formula(sprintf("y ~ age + sex + ethnicity + marital_status + education  + hhtype + hhsize + 
    hhhomeownership + hhincomesource + hhincome + hhassets + oad + %s - 1",
    paste(paste("x", 1:n_ogc, sep=""), collapse=" + ")))
  X <- sparse.model.matrix(formula, data.population)
  return(X)
}