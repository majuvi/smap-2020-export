# Given features X and outcome y with missing values (NA), predict the missing values and return mean(y) for each group 
# class.index is a mapping of each row of X to one or more groups (index -> class) to calculate mean(y) by group 
model_predict_bootstrap <- function (X, y, class.index, prediction.intervals="none", n.bootstrap = 25, n.bootstrap.inner=40, eta=0.1) {

  # Split the data into train and test set
  observation <- !is.na(y)
  X.train <- X[observation,]
  y.train <- y[observation]
  
  # Save all individual predictions temporarily here
  class.index$y_pred <- NA
  
  # Estimate the number of trees
  print("Estimating the number of trees (5-CV)...")
  start = Sys.time()
  set.seed(42)
  model <- xgb.cv(list(objective="binary:logistic", eval_metric="rmse"), data=X.train, label=y.train, 
    eta=eta, nrounds=1000, verbose=0, nfold=5, stratified=T, early_stopping_rounds=10)
  optimal_nrounds = copy(model$best_iteration)
  print(sprintf("Optimal number of rounds: %s", optimal_nrounds))
  end = Sys.time()
  print(end-start)
  
  # Train a full model on original data to predict the probabilities
  print("Training the model...")
  start = Sys.time()
  model <- xgboost(data=X.train, label=y.train, objective="binary:logistic", eta=eta, 
    nrounds=optimal_nrounds, verbose=0)
  # Predict all labels and fill the observed training labels 
  y.pred <- predict(model, X)
  y.pred[observation] <- y.train
  # Map predictions to the multiple classes and save the original model predictions
  class.index$y_pred <- y.pred[class.index$index]
  pred.class         <- class.index %>% summarise.(N = .N, n = sum(y_pred), .by = class, .sort=T)
  stats              <- pred.class %>% mutate.(y.model = n / N) %>% select.(class, N, y.model)
  end = Sys.time()
  print(end-start)
  
  if (prediction.intervals == "outcome") {
    # Prediction intervals of type "outcome" assume that the model is correct
    library(poisbinom) # If p is a vector of probabilities of i.d. Bernoulli trials, the sum is Poisson Binomial
    print("Calculating statistics...")
    start = Sys.time()
    # Calculate prediction intervals from implied outcome probabilities 
    stats.outcomes <- class.index %>% summarise.(N = .N, n = sum(y_pred),
      n.outcomes.lower=qpoisbinomzero(y_pred, 0.025),
      n.outcomes.upper=qpoisbinomzero(y_pred, 0.975), .by = class, .sort=T)
    # Merge together to get predicted counts in original model and implied outcomes
    stats = stats %>% 
      left_join.(stats.outcomes, by="class") %>% mutate.(
        y.outcomes.lower   = n.outcomes.lower / N,
        y.outcomes.upper   = n.outcomes.upper / N
      ) %>% select.(class, N, y.model, y.outcomes.lower, y.outcomes.upper) 
    end = Sys.time()
    print(end-start)
  } else if (prediction.intervals == "full") {
    # Otherwise we bootstrap resample the data set, fit model to each bootstrap and sample the predicted outcome
    print("Bootstrapping...")
    # This is where bootstrap and sampled outcome predictions are saved
    pred.bootstrap <- class.index %>% summarise.(N = .N, .by = class, .sort=T)
    pred.outcomes  <- class.index %>% summarise.(N = .N, .by = class, .sort=T)
    
    set.seed(42)
    # Bootstrap resample the data
    for (n in 1:n.bootstrap) {
      print(sprintf("Bootstrap %d", n))
      
      # Bootstrap resample a new training set
      indices <- sample(1:nrow(X.train), replace=T)
      X.bootstrap <- X.train[indices,]
      y.bootstrap <- y.train[indices]
      
      # Train on boostrapped sample
      print("Training on bootstrapped sample...")
      start = Sys.time()
      model <- xgboost(objective="binary:logistic", data=X.bootstrap, label=y.bootstrap, eta = eta,
        nrounds=optimal_nrounds, verbose=0)
      # Predict all labels and fill the observed training labels 
      y.pred <- predict(model, X)
      y.pred[observation] <- y.train
      # Map predictions to the multiple classes and save the bootstrap sample results
      class.index$y_pred <- y.pred[class.index$index]
      pred.class         <- class.index %>% summarise.(n = sum(y_pred), .by = class)
      pred.bootstrap     <- pred.bootstrap %>% left_join.(pred.class, by = "class")
      end = Sys.time()
      print(end-start)
      
      # Inner bootstrap loop
      for (m in 1:n.bootstrap.inner) {
        print(sprintf("Sample outcome %d", m))
        #Sample from the predicted probabilities 
        y.sample <- rbinom(length(y.pred), 1, prob=y.pred)
        # Count the sum and map to classes
        class.index$y_pred <- y.sample[class.index$index]
        pred.class         <- class.index %>% summarise.(n = sum(y_pred), .by = class)
        pred.outcomes      <- pred.outcomes %>% left_join.(pred.class, by = "class")
      }
    }
    
    print("Calculating statistics...")
    start = Sys.time()
    # Statistics from re-training the model in bootstrap samples
    stats.bootstrap <- pred.bootstrap %>% select.(-N) %>%
      mutate_rowwise.(n.bootstrap.mean = mean(c_across.(where(is.numeric))), 
        n.bootstrap.sd   = sd(c_across.(where(is.numeric)))) %>%
      select.(class, n.bootstrap.mean, n.bootstrap.sd)
    # Statistics from re-training the model in bootstrap samples and sampling the outcome
    stats.outcomes <- pred.outcomes %>% select.(-N) %>%
      mutate_rowwise.(n.outcomes.sd    = sd(c_across.(where(is.numeric))), 
        n.outcomes.lower = quantile(c_across.(where(is.numeric)), 0.025, na.rm=T), 
        n.outcomes.upper = quantile(c_across.(where(is.numeric)), 0.975, na.rm=T)) %>%
      select.(class, n.outcomes.sd, n.outcomes.lower, n.outcomes.upper)
    # Merge together to get predicted counts in original model, bootstrap and sampled outcomes
    stats = stats %>% 
      left_join.(stats.bootstrap, by="class") %>% 
      left_join.(stats.outcomes, by="class") %>% mutate.(
        y.bootstrap.mean = n.bootstrap.mean / N,
        y.bootstrap.sd   = n.bootstrap.sd / N,
        y.outcomes.sd      = n.outcomes.sd / N,
        y.outcomes.lower   = n.outcomes.lower / N,
        y.outcomes.upper   = n.outcomes.upper / N
      ) %>% select.(class, N, y.model, y.bootstrap.mean, y.bootstrap.sd, 
        y.outcomes.sd, y.outcomes.lower, y.outcomes.upper) 
    end = Sys.time()
    print(end-start)
  }
  
  return(stats)
}
