# SMAP 2020 Export

Source codes with examples of how to use the new XGBoost model to produce small area estimates.

## Getting started

- See [Scripts/00_Example_SMAP2020.R](Scripts/00_Example_SMAP2020.R) for a simple example that predicts the prevalence of "drinker" in each neighbourhood.
- See [Scripts/01_All_predictions_SMAP2020.R](Scripts/01_All_predictions_SMAP2020.R) for a full calculation of all small area estimates for all indicators.


## Authors 

The new XGBoost model was created by Markus Viljanen (markus.viljanen@rivm.nl) and Lotta Meijerink (lotta.meijerink@rivm.nl). The original STAR model was created by Jan van de Kassteele (jan.van.de.kassteele@rivm.nl), see also https://github.com/kassteele/SMAP.
